import * as S from "./styles.js";
import logo from "../../assets/cilia-blue.png";

const Sidebar = ({ isMenuOpen, onMenuToggle }) => {
  return (
    <S.Container
      className={`sidebar-menu${isMenuOpen === true ? " open" : ""}`}
    >
      <S.ContainerLogo onClick={onMenuToggle}>
        <S.ImageUsers src={logo} />
      </S.ContainerLogo>
      <S.ContainerButton onClick={onMenuToggle}>
        <span
          className="iconify"
          data-icon="akar-icons:three-line-horizontal"
          data-inline="false"
        />
      </S.ContainerButton>

      <S.ContainerBody>
        <S.List>
          <S.ListUnique>
            <span
              className="iconify"
              data-icon="mdi:update"
              data-inline="true"
            />
            <small>Dashboard</small>
          </S.ListUnique>
          <S.ListUnique>
            <span
              className="iconify"
              data-icon="mdi:clipboard-text-outline"
              data-inline="true"
            />
            <small>Atendimento (108)</small>
          </S.ListUnique>
          <S.ListUnique>
            <span
              className="iconify"
              data-icon="mdi:alarm-check"
              data-inline="true"
            />
            <small>Reparos (42)</small>
          </S.ListUnique>
          <S.ListUnique>
            <span className="iconify" data-icon="mdi:car" data-inline="true" />
            <small>Carros Reserva (56)</small>
          </S.ListUnique>
          <S.ListUnique disabled>
            <span
              className="iconify"
              data-icon="mdi:wrench"
              data-inline="true"
            />
            <small>Oficinas (42)</small>
          </S.ListUnique>
          <S.ListUnique disabled>
            <span
              className="iconify"
              data-icon="heroicons-solid:users"
              data-inline="true"
            />
            <small>Usuários (42)</small>
          </S.ListUnique>
          <S.ListUnique disabled>
            <span
              className="iconify"
              data-icon="fluent:contact-card-group-24-filled"
              data-inline="true"
            />
            <small>Grupos(8)</small>
          </S.ListUnique>
          <S.ListUnique disabled>
            <span
              className="iconify"
              data-icon="fluent:calendar-star-24-filled"
              data-inline="true"
            />
            <small>Permissões (4)</small>
          </S.ListUnique>
          <S.ListUnique disabled>
            <span
              className="iconify"
              data-icon="uiw:setting"
              data-inline="true"
            />
            <small>Configurações</small>
          </S.ListUnique>
        </S.List>
      </S.ContainerBody>
    </S.Container>
  );
};
export default Sidebar;
