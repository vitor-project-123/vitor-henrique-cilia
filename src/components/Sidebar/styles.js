import styled, { css } from "styled-components";

export const Container = styled.div`
  width: 300px;
  height: 100%;
  position: fixed;
  top: 0;
  left: -300px;
  -webkit-transform: translateX(0);
  transform: translateX(0);
  -webkit-transition: 0.3s ease all;
  transition: 0.3s ease all;
  background: #fff;
  z-index: 1;
  border-right: 1px solid #e7e9eb;
  &.open {
    -webkit-transform: translateX(300px);
    transform: translateX(300px);
  }

  @media (max-width: 620px) {
    width: 250px;
  }

  @media (max-width: 480px) {
    width: 75px;
  }
`;
export const ContainerLogo = styled.div`
  color: white;
  float: left;

  @media (max-width: 480px) {
    display: none;
  }
`;


export const ContainerButton = styled.div`
  color: white;
  float: right;
  margin: 15px 10px 0 0;
  cursor: pointer;
  @media (max-width: 480px) {
    margin: 10px 25px 0 0;
  }

  .iconify {
    font-size: 1.5rem;
    color: var(--header);
    cursor: pointer;
  }
`;
export const ContainerBody = styled.div`
  margin-top: 50px;
`;



export const List = styled.ul`
  padding: 10px 8px;
  list-style: none;
  width: 100%;
  height: 100%;
  padding-left: 20px;
`;

export const ListUnique = styled.li`
  width: 90%;
  padding: 3px 0 0 3px;
  text-align: start;
  background-color: white;
  border-bottom: 1px solid #e7e9eb;
  padding-bottom: 10px;
  cursor: pointer;
  margin-bottom: 5px;

  ${(props) =>
    props.disabled &&
    css`
      pointer-events: none;
      opacity: 0.5;
    `}

  .iconify {
    font-size: 1.4rem;
    color: var(--header);
    padding-top: 5px;
  }

  small {
    font-size: 1.3rem;
    font-weight: 600;
    color: #596270;
    padding-left: 10px;
  }
  @media (max-width: 480px) {
    width: 60%;

    small {
      display: none;
    }
  }
`;

export const ImageUsers = styled.img`
  margin-left: 10px;
`;