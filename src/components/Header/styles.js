import styled, { css } from "styled-components";

export const Container = styled.div`
  height: 55px;
  width: 100%;

  background: var(--header);
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #e7e9eb;

  @media (max-width: 480px) {
    width: 100%;
  }
`;

export const IconCash = styled.img`
  width: 20px;
`;

export const Column = styled.div`
  display: flex;
  align-items: center;
  width: 55%;
  @media (max-width: 1068px) {
    width: 35%;
  }

  ${(props) =>
    props.middle &&
    css`
      justify-content: flex-start;
      width: 45%;

      @media (max-width: 1068px) {
        width: 65%;
      }
    `}

  @media (max-width: 375px) {
    margin-top: 1rem;
    justify-content: center;
  }
`;

export const ContainerSide = styled.div`
  height: 3.5rem;
  width: 3.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 1rem;
  .iconify {
    font-size: 1.5rem;
    color: white;
    cursor: pointer;
  }
`;

export const ContainerIconLogoCash = styled.div`
  height: 3.5rem;
  width: 3.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 1rem;
  .iconify {
    font-size: 1.5rem;
    color: white;
    cursor: pointer;
  }

  @media (max-width: 480px) {
    display: none;
  }
`;
export const ContainerIconHelp = styled.div`
  height: 30px;
  width: 20%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 1rem;
  .iconify {
    font-size: 1.5rem;
    color: #145a7f;
    cursor: pointer;
  }
  small {
    color: white;
    margin-left: 10px;
    font-weight: 600;
  }
  @media (max-width: 375px) {
    small {
      display: none;
    }
  }
`;

export const ContainerIconChat = styled.div`
  height: 30px;
  width: 40%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 1rem;
  border-right: 1px solid #145a7f;

  border-left: 1px solid #145a7f;
  .iconify {
    font-size: 1.5rem;
    color: #145a7f;
    cursor: pointer;
  }
  small {
    color: white;
    margin-left: 10px;
    font-weight: 600;
  }

  @media (max-width: 375px) {
    small {
      display: none;
    }
  }
`;

export const ContainerIconSelect = styled.div`
  height: 30px;
  width: 40%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-right: 0.5rem;
  cursor: pointer;

  .iconify {
    font-size: 2rem;
    color: white;
  }
  span {
    color: white;
    margin-left: 5px;
    font-weight: 600;
    font-size: 14px;
  }
  small {
    color: white;
    margin-left: 5px;
    font-weight: 600;
    font-size: 10px;
  }
`;

export const ContainerText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: right;
  margin-right: 10px;

  @media (max-width: 628px) {
    span {
      display: none;
    }
  }

  @media (max-width: 480px) {
    small {
      display: none;
    }
  }
`;

export const Select = styled.button`
  padding: 0 1rem;
  height: 4rem;

  border-radius: 0.5rem;
  color: #244339;

  @media (max-width: 375px) {
    width: 20rem;
  }
`;

export const ImageUsers = styled.img`
  margin-left: 30px;
`;
