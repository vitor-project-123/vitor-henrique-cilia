import React, { useState } from "react";
import SelectExample from "../Actions";
import logo from "../../assets/cilia-white.png";

import * as S from "./styles";

export default function Header({ onMenuToggle }) {
  const [active, setActive] = useState(false);
  return (
    <S.Container>
      <S.Column>
        <S.ContainerSide onClick={onMenuToggle}>
          <span
            className="iconify"
            data-icon="akar-icons:three-line-horizontal"
            data-inline="false"
          />
        </S.ContainerSide>
        <S.ContainerIconLogoCash>
          <S.ImageUsers src={logo} />
        </S.ContainerIconLogoCash>
      </S.Column>
      <S.Column middle>
        <S.ContainerIconHelp>
          <span
            className="iconify"
            data-icon="bx:bxs-help-circle"
            data-inline="false"
          />
          <small>Ajuda</small>
        </S.ContainerIconHelp>
        <S.ContainerIconChat>
          <span
            className="iconify"
            data-icon="bi:chat-left-text-fill"
            data-inline="false"
          />
          <small>Suporte via Chat</small>
        </S.ContainerIconChat>
        <S.ContainerIconSelect onClick={() => setActive((prev) => !prev)}>
          <S.ContainerText>
            <span>Diego . Auto/RE - TESTE</span>
            <small>ADMINISTRADOR</small>
          </S.ContainerText>
          <span
            className="iconify"
            data-icon="mdi:account-circle"
            data-inline="false"
          />
          <span
            className="iconify"
            data-icon="mdi:home-circle"
            data-inline="false"
          />
          <SelectExample open={active} />
        </S.ContainerIconSelect>
      </S.Column>
    </S.Container>
  );
}
