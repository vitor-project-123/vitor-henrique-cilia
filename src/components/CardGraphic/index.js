import React from "react";
import * as S from "./styles";

import Graphic from "../Graphic";

export default function CardGraphic() {
  return (
    <S.Container>
      <S.ContainerBox>
        <S.ContainerIconLogo>
          <span
            className="iconify"
            data-icon="mdi:wrench"
            data-inline="false"
          />
          <small>MONTADORAS</small>
        </S.ContainerIconLogo>
        <S.ContainerBody>
          <Graphic />
        </S.ContainerBody>
      </S.ContainerBox>
    </S.Container>
  );
}
