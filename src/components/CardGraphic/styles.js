import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: 350px;

`;

export const ContainerBox = styled.div`
  flex-direction: column;
  margin: 20px 20px 0 20px;
  background: #fff;
  width: 100%;
  border-radius: 5px;
  border: 1px solid #e7e9eb;
`;

export const ContainerIconLogo = styled.div`
  height: 10%;
  width: 100%;
  display: flex;
  flex-direction: row;
  margin: 10px 10px 0 15px;

  .iconify {
    font-size: 1.5rem;
    color: var(--header);
    margin-right: 5px;
  }

  small {
    font-size: 16px;
    font-weight: 600;
  }
`;

export const ContainerBody = styled.div`
  height: 80%;
  width: 98%;
  margin-top: 10px;
  border-top: 1px solid #e7e9eb;
  padding-top: 10px;
`;
