/* eslint-disable jsx-a11y/iframe-has-title */
import React from "react";

import * as S from "./styles";

export default function CardLocalization() {
  return (
    <S.Container>
      <S.ContainerBox>
      <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3821.8468115861197!2d-49.26586628565048!3d-16.68454744992028!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef167fe77afad%3A0x616a06459c0a17c8!2sCilia%20Tecnologia!5e0!3m2!1spt-BR!2sbr!4v1630710350834!5m2!1spt-BR!2sbr"
          width="100%"
          height="100%"
          allowfullscreen=""
          loading="lazy"
        ></iframe>
      </S.ContainerBox>
    </S.Container>
  );
}
