import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 50%;
  height: 450px;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const ContainerBox = styled.div`
  display: flex;
  margin: 20px 20px 10px 10px;
  background: #fff;
  width: 100%;
  border: 1px solid #e7e9eb;
`;


