import React from "react";
import * as S from "./styles";

const SelectExample = ({ open }) => {
  return (
    <>
      {open ? (
        <S.Container>
          <S.List>
            <S.ListUnique>Meu Perfil</S.ListUnique>
            <S.ListUnique>Configurações</S.ListUnique>
            <S.ListUnique exit>Sair</S.ListUnique>
          </S.List>
        </S.Container>
      ) : null}
    </>
  );
};
export default SelectExample;
