import styled, { css } from "styled-components";

export const Container = styled.div`
  position: absolute;
  top: 30px;
  right: 10px;
  text-align: right;
  z-index: 1;
`;

export const List = styled.ul`
  padding: 5px 8px;
  margin-top: 25px;
  list-style: none;
  width: 200px;
  height: 100%;
  border: 1px solid #e7e9eb;
  background-color: white;
  border-radius: 5px;
  box-shadow: 1px 1px 1px #e7e9eb;
`;

export const ListUnique = styled.li`
  padding: 5px 0 0 3px;
  text-align: start;
  background-color: white;
  border-bottom: 1px solid #e7e9eb;
  font-size: 14px;
  font-weight: 600;
  color: gray;
  padding-bottom: 10px;
  margin-top: 5px;


  ${(props) =>
    props.exit &&
    css`
      color: red;
    `}
`;
