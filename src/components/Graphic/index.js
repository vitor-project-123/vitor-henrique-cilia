import {
  ResponsiveContainer,
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
} from "recharts";

const data = [
  { name: "BMW", Porcentagem: 4},
  { name: "CHEVROLET", Porcentagem: 8 },
  { name: "CITROEN", Porcentagem: 20 },
  { name: "FIAT", Porcentagem: 29 },
  { name: "FORD", Porcentagem: 9 },
  { name: "HONDA", Porcentagem: 19 },
  { name: "HYUNDAI", Porcentagem: 13 },
  { name: "IVECO", Porcentagem: 16 },
  { name: "JEEP", Porcentagem: 13 },
  { name: "RENAULT", Porcentagem: 9 },
];

export default function Graphic() {
  return (
    <ResponsiveContainer>
      <LineChart data={data}>
        <Line type="monotone" dataKey="Porcentagem" stroke="#ee4146" />
        <CartesianGrid />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
      </LineChart>
    </ResponsiveContainer>
  );
}
