import React from "react";
import * as S from "./styles";

export default function CardInformation() {
  return (
    <S.Container>
      <S.ContainerBox>
        <S.ContainerIconLogo>
          <span
            className="iconify"
            data-icon="bx:bxs-car"
            data-inline="false"
          />

          <small>VEÍCULO</small>
        </S.ContainerIconLogo>
        <S.ContainerBody>
          <S.ContainerBodyItem>
            <span
              className="iconify"
              data-icon="simple-icons:volkswagen"
              data-inline="false"
            />
            <small>
              VOLKSWAGEN POLO (2018) COMFORTLINE 200 TSI 1.0 TSI 2018
            </small>
          </S.ContainerBodyItem>
          <S.ContainerBodyItem smoke>
            <span
              className="iconify"
              data-icon="simple-icons:volkswagen"
              data-inline="false"
            />
            <small>
              VOLKSWAGEN POLO (2018) COMFORTLINE 200 TSI 1.0 TSI 2018
            </small>
          </S.ContainerBodyItem>
          <S.ContainerBodyItem>
            <span
              className="iconify"
              data-icon="simple-icons:volkswagen"
              data-inline="false"
            />
            <small>
              VOLKSWAGEN POLO (2018) COMFORTLINE 200 TSI 1.0 TSI 2018
            </small>
          </S.ContainerBodyItem>
          <S.ContainerBodyItem smoke>
            <span
              className="iconify"
              data-icon="simple-icons:volkswagen"
              data-inline="false"
            />
            <small>
              VOLKSWAGEN POLO (2018) COMFORTLINE 200 TSI 1.0 TSI 2018
            </small>
          </S.ContainerBodyItem>
        </S.ContainerBody>
      </S.ContainerBox>
    </S.Container>
  );
}
