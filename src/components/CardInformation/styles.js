import styled, { css } from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 50%;
  height: 450px;
  @media (max-width: 768px) {
    width: 100%;
  }
`;
export const ContainerBox = styled.div`
  flex-direction: column;
  margin: 20px 10px 0 20px;
  background: #fff;
  width: 100%;
  border-radius: 5px;
`;

export const ContainerIconLogo = styled.div`
  height: 10%;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: row;
  margin: 10px 10px 0 15px;

  .iconify {
    font-size: 1.5rem;
    color: var(--header);
    cursor: pointer;
    margin-right: 5px;
  }

  small {
    font-size: 16px;
    font-weight: 600;
  }
`;

export const ContainerBody = styled.div`
  height: 90%;
  margin: 10px 10px 0 15px;
  border-top: 1px solid #e7e9eb;
  padding-top: 5px;
  flex-direction: column;
`;

export const ContainerBodyItem = styled.div`
  height: 20%;
  border-bottom: 1px solid #e7e9eb;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  .iconify {
    font-size: 2rem;
    color: var(--header);
    cursor: pointer;
    margin-right: 5px;
  }

  small {
    font-size: 16px;
    font-weight: 600;
  }
  ${(props) =>
    props.smoke &&
    css`
      background-color: #f1f4f6;
    `}
`;
