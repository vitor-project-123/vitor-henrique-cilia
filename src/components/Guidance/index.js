import React from "react";
import * as S from "./styles";

export default function Guidance({ teste = "PÁGINA TESTE" }) {
  return (
    <S.Container>
      <S.SpanHome>INÍCIO</S.SpanHome>
      <S.SpanIcon>{">"}</S.SpanIcon>
      <S.SpanPage>{teste}</S.SpanPage>
    </S.Container>
  );
}
