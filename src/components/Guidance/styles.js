import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  height: 50px;
  width: 100%;
  background-color: #fff;
  padding-left: 20px;
  align-items: center;
  border-bottom: 1px solid #f1f4f6;
  font-size: 14px;
  font-weight: 700;
  color: #808792;
`;

export const SpanHome = styled.span`

`;

export const SpanIcon = styled.span`
  color: grey;
  margin: 0 5px 0 5px;
`;

export const SpanPage = styled.span`

`;
