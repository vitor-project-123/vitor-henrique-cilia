import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Container2 = styled.div`
  display: flex;
  flex-direction: row;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Card = styled.div`
  padding: 3rem;

  min-height: 131px;
  width: 370px;

  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 1516px) {
    width: 100%;
  }
`;
