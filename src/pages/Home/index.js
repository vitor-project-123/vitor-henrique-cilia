import React from "react";
import CardGraphic from "../../components/CardGraphic";
import CardInformation from "../../components/CardInformation";
import CardLocalization from "../../components/CardLocalization";
import * as S from "./styles";

export default function Home() {
  return (
    <S.Container>
      <CardGraphic />
      <S.Container2>
        <CardInformation />
        <CardLocalization />
      </S.Container2>
    </S.Container>
  );
}
