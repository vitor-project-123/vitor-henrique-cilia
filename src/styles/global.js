import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`

:root {
    --background: #f1f4f6;
    --backgroundCard: #ffffff;
    --header: #29b6ff;
    --icons: #29b6ff;
    --black: #414e5d;
    --highlight: #e20e8d;
    --medium: 3rem;
  }

*{
  margin: 0;;
  padding: 0%;
  box-sizing: border-box;
}

html {
  font-size: 100%;
}

html, body, #__next {
  height: 100%;
  background: var(--background);
  color: var(--black);

}

body{
  font-family: --apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif
}

p{
  font-size: 2rem;
  line-height: var(--medium);
}

a{
  color: var(--highlight);
}

`;

export default GlobalStyles;

//root = raiz
