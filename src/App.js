import React, { useState } from "react";
import GlobalStyles from "./styles/global";
import Sidebar from "./components/Sidebar";
import Home from "./pages/Home";
import Header from "../src/components/Header";
import Guidance from "../src/components/Guidance";

function App() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  function toggleMenu() {
    setIsMenuOpen((prev) => !prev);
  }
  return (
    <div>
      <Sidebar
        isMenuOpen={isMenuOpen}
        onMenuToggle={toggleMenu}
      />
      <Header onMenuToggle={toggleMenu} />
      <Guidance />
      <Home />
      <GlobalStyles />
    </div>
  );
}

export default App;
